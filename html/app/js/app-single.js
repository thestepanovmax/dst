﻿(function () {
	'use strict';

	$().ready(init);

	function init() {
		galleria();
		sidebarHover();
		toggleButton();
		animateScroll();
		clientsGallery();
		toggleDescription();
		clientsPopularGallery();
	}

	function sidebarHover() {
		$('#sidebar').hover(function(){
			$(this).addClass('expanded');
		});
	}

	function toggleButton() {
		$('#toggleButton').click(function(){
			$(this).toggleClass('active');
			$('.header-mobile').toggleClass('active');
		});
	}

	function clientsPopularGallery() {
	  $('#clients_popular').slick({
		  infinite: 						true,
		  autoplay: 						true,
			unslick: 							true,
			arrows: 							false,
			dots: 								false,
			autoplaySpeed: 				3000,
			slidesToShow: 				5,
			responsive: [
				{
					breakpoint: 		1024,
					settings: {
						vertical: 			false,
						slidesToShow: 	3
					}
				},
				{
					breakpoint: 		768,
					settings: {
						vertical: 			false,
						slidesToShow:		1
					}
				}
			]
		});
	}

	function clientsGallery() {
		$('#clients').slick({
		  infinite: 						true,
		  autoplay: 						true,
			dots: 								false,
			arrows: 							false,
			vertical: 						true,
			autoplaySpeed: 				3000,
			slidesToShow: 				5,
			responsive: [
				{
					breakpoint: 		1024,
					settings: {
						vertical: 			false,
						slidesToShow: 	3
					}
				},
				{
					breakpoint: 		768,
					settings: {
						vertical: 			false,
						slidesToShow:		1
					}
				}
			]
		});
	}

	function animateScroll() {
		$("[id^='link']").click(function(){
			console.log( 'asd' );
			var $id = $(this).attr('id').replace('link', 'section');
			console.log( $id );
			$('#' + $id).animatescroll({
				scrollSpeed: 1500,
				padding: 20,
				easing:'easeInOutCirc'
			});
		});
	}

	function toggleDescription() {
		$('#openTextButton').click(function(){
			$('#sectionDescription').toggleClass('open');
		});
	}

	function galleria() {
		Galleria.loadTheme('js/galleria/themes/classic/galleria.classic.min.js');
		Galleria.run('#galleria, #galleria2');
	}
	
})();
