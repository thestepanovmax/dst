﻿(function () {
	'use strict';

	$().ready(init);

	function init() {
		sidebarHover();
		toggleButton();
		clientsPopularGallery();
	}

	function sidebarHover() {
		$('#sidebar').hover(function(){
			$(this).addClass('expanded');
		});
	}

	function toggleButton() {
		$('#toggleButton').click(function(){
			$(this).toggleClass('active');
			$('.header-mobile').toggleClass('active');
		});
	}

	function clientsPopularGallery() {
	  $('#clients_popular').slick({
		  infinite: 						true,
		  autoplay: 						true,
			unslick: 							true,
			arrows: 							false,
			dots: 								false,
			autoplaySpeed: 				3000,
			slidesToShow: 				5,
			responsive: [
				{
					breakpoint: 		1024,
					settings: {
						vertical: 			false,
						slidesToShow: 	3
					}
				},
				{
					breakpoint: 		768,
					settings: {
						vertical: 			false,
						slidesToShow:		1
					}
				}
			]
		});
	}
	
})();
