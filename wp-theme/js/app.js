$(document).ready(function(){
	$('#sidebar').hover(function(){
    $(this).addClass('expanded');
  });
  $('#toggleButton').click(function(){
    $(this).toggleClass('active');
    $('.header-mobile').toggleClass('active');
  });
  $('#clients_popular').slick({
	  infinite: 						true,
	  autoplay: 						true,
		unslick: 							true,
		arrows: 							false,
		dots: 								false,
		autoplaySpeed: 				3000,
		slidesToShow: 				3,
	  responsive: [
			{
				breakpoint: 		10000,
				settings: 			"unslick"
			},
			{
				breakpoint: 		1024,
				settings: {
					unslick: 				false,
					slidesToShow: 	3
				}
			},
			{
				breakpoint: 		768,
				settings: {
					unslick: 				false,
					slidesToShow:		1
				}
			}
		]
	});

});