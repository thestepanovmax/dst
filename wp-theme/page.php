<?php
/*
Template Name: Home template
*/
get_header(); ?>

	<?php include 'svg/svg_catalog.php' ?>

	<?php include 'sidebar.php'; ?>
	
    <!-- Main content -->
    <div class="main-content">
		
			<?php include 'clients.php'; ?>
			
      <!-- Column -->
      <div class="column column_center">
			
				<!-- Widget center -->
				<div class="widget widget_center">
					<!-- Search -->
					<?php echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); ?>
				</div>
				
				<!-- Widget center -->
				<div class="widget widget_center">
					<h2 class="catalog__title">
						<?php the_title(); ?>
					</h2>
					<?php the_content(); ?>
        </div>
      </div>
			
      <!-- Widget news -->
			<?php include 'news.php'; ?>
			
    </div>
<?php get_footer(); ?>
