var gulp = require('gulp'),
	less = require('gulp-less'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	autoprefixer = require('gulp-autoprefixer'),
	notify = require('gulp-notify'),
	minifyCSS = require('gulp-minify-css'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	mainBowerFiles = require('main-bower-files'),
	zip = require('gulp-zip'),
	bowerFiles = mainBowerFiles();

/******************************
 * Default task
 ******************************/
gulp.task('default', [
	'bower-me',
	// 'pluginsConcat',
	'jsConcat',
	'less',
	'watch'
]);

/******************************
 * Build task
 ******************************/
gulp.task('build', [
	'bower-me',
	// 'pluginsConcat',
	'jsConcatBuild',
	'less'
]);

/******************************
 * Bower files task
 ******************************/
gulp.task('bower-me', function () {
	console.info('********** Bower Files [' + bowerFiles.length + '] **********');
	console.info(bowerFiles);
});

/******************************
 * Watch
 ******************************/
gulp.task('watch', function () {
	gulp.watch(['less/**/*.less'], ['less']);
	gulp.watch('js/**/*.js', ['jsConcat']);
});

/******************************
 * ZIP
 ******************************/
gulp.task('zip', function () {
	gulp.src([
		'../**/*',
		'!../curago-theme.zip',
		'!../gulp',
		'!../gulp/**/*'
		])
		.pipe(plumber())
		.pipe(zip('curago-theme.zip'))
		.pipe(gulp.dest('../'));
});

/******************************
 * Less
 ******************************/
gulp.task('less', function () {
	gulp.src(['less/style.less'])
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(less())
		.on('error', notify.onError(function (error) {
			return '\nAn error occurred while compiling css.\nLook in the console for details.\n' + error;
		}))
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('../'));
});

/******************************
 * Less build
 ******************************/
gulp.task('lessBuild', function () {
	gulp.src(['less/style.less'])
		.pipe(plumber())
		.pipe(less())
		.on('error', notify.onError(function (error) {
			return '\nAn error occurred while compiling css.\nLook in the console for details.\n' + error;
		}))
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
			cascade: false
		}))
		.pipe(minifyCSS({
			keepBreaks: false,
			keepSpecialComments: true,
			benchmark: false,
			debug: true
		}))
		.pipe(gulp.dest('../'));
});

/* ==========================================================
 * JS plugins
 * ===========================================================
 */
gulp.task('pluginsConcat', function() {
	gulp.src(bowerFiles)
		.pipe(concat('plugins.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('../js'));
});

/* ==========================================================
 * JS concat
 * ===========================================================
 */
gulp.task('jsConcat', function() {
	gulp.src(['js/**/*.js'])
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(uglify())
		.on('error', notify.onError(function(error) {
			return '\nAn error occurred while uglifying js.\nLook in the console for details.\n' + error;
		}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('../js'));
});

/* ==========================================================
 * JS concat build
 * ===========================================================
 */
gulp.task('jsConcatBuild', function() {
	gulp.src(['js/**/*.js'])
		.pipe(plumber())
		.pipe(concat('app.js'))
		.pipe(uglify())
		.on('error', notify.onError(function(error) {
			return '\nAn error occurred while uglifying js.\nLook in the console for details.\n' + error;
		}))
		.pipe(gulp.dest('../js'));
});
