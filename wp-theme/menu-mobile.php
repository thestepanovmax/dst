  <!-- Header mobile -->
  <header class="header-mobile">
    <!-- Logo -->
    <a href="#" title="Логотип" class="logo logo_mobile">
      <svg class="logo__svg">
        <use xlink:href="#icon-logo-mobile"></use>
      </svg>
    </a>
    <!-- Button toggle -->
    <button class="btn btn_toggle header-mobile__btn" id="toggleButton">
      <span class="btn__line"></span>
      <span class="btn__line"></span>
      <span class="btn__line"></span>
      <span class="btn__line"></span>
    </button>
    <!-- Menu -->
    <ul class="list list_vertical menu menu_mobile">
      <li class="list__item">
        <a class="menu__link" href="#" title="Главная">
          <svg class="menu__icon">
            <use xlink:href="#icon-home"></use>
          </svg>
          <span class="menu__text">
            Главная
          </span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="О Деловой справке">
          <svg class="menu__icon">
            <use xlink:href="#icon-businessman"></use>
          </svg>
          <span class="menu__text">О Деловой справке</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="Каталог">
          <svg class="menu__icon">
            <use xlink:href="#icon-layers"></use>
          </svg>
          <span class="menu__text">Каталог</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="Новости">
          <svg class="menu__icon">
            <use xlink:href="#icon-rss"></use>
          </svg>
          <span class="menu__text">Новости</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="Стать участником">
          <svg class="menu__icon">
            <use xlink:href="#icon-plus"></use>
          </svg>
          <span class="menu__text">Стать участником</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="Личный кабинет">
          <svg class="menu__icon">
            <use xlink:href="#icon-login"></use>
          </svg>
          <span class="menu__text">Личный кабинет</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="Тарифы">
          <svg class="menu__icon">
            <use xlink:href="#icon-dollar"></use>
          </svg>
          <span class="menu__text">Тарифы</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="FAQ">
          <svg class="menu__icon">
            <use xlink:href="#icon-faq"></use>
          </svg>
          <span class="menu__text">FAQ</span>
        </a>
      </li>
      <li class="list__item">
        <a class="menu__link" href="#" title="Контакты">
          <svg class="menu__icon">
            <use xlink:href="#icon-contacts"></use>
          </svg>
          <span class="menu__text">Контакты</span>
        </a>
      </li>
    </ul>
    <!-- Developer -->
    <div class="developer developer_mobile">
      <div class="developer__info"></div>
      <a href="#" title="" class="developer__link">
        <svg class="developer__icon">
          <use xlink:href="#icon-code"></use>
        </svg>
        <span class="developer__text">Разработчик сайта</span>
      </a>
    </div>
  </header>