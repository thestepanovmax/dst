
<!-- Menu -->
<ul class="list list_vertical menu menu_sidebar">
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/" title="Главная">
      <svg class="menu__icon">
        <use xlink:href="#icon-home"></use>
      </svg>
      <span class="menu__text">
        Главная
      </span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/about-us/" title="О Деловой справке">
      <svg class="menu__icon">
        <use xlink:href="#icon-businessman"></use>
      </svg>
      <span class="menu__text">О Деловой справке</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/catalog/" title="Каталог">
      <svg class="menu__icon">
        <use xlink:href="#icon-layers"></use>
      </svg>
      <span class="menu__text">Каталог</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/news/" title="Новости">
      <svg class="menu__icon">
        <use xlink:href="#icon-rss"></use>
      </svg>
      <span class="menu__text">Новости</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/register/" title="Стать участником">
      <svg class="menu__icon">
        <use xlink:href="#icon-plus"></use>
      </svg>
      <span class="menu__text">Стать участником</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/wp-admin/" title="Личный кабинет">
      <svg class="menu__icon">
        <use xlink:href="#icon-login"></use>
      </svg>
      <span class="menu__text">Личный кабинет</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/tarifs/" title="Тарифы">
      <svg class="menu__icon">
        <use xlink:href="#icon-dollar"></use>
      </svg>
      <span class="menu__text">Тарифы</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/faq/" title="FAQ">
      <svg class="menu__icon">
        <use xlink:href="#icon-faq"></use>
      </svg>
      <span class="menu__text">FAQ</span>
    </a>
  </li>
  <li class="list__item">
    <a class="menu__link" href="<?php echo site_url(); ?>/contacts/" title="Контакты">
      <svg class="menu__icon">
        <use xlink:href="#icon-contacts"></use>
      </svg>
      <span class="menu__text">Контакты</span>
    </a>
  </li>
</ul>