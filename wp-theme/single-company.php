<?php
/*
Template Name: Home template
*/
get_header(); ?>

	<?php include 'svg/svg_catalog.php' ?>

	<?php include 'sidebar.php'; ?>
	
    <!-- Main content -->
    <div class="main-content">
		
			<?php include 'clients.php'; ?>
			
      <!-- Column -->
      <div class="column column_center">
			
        <!-- Search -->
				<?php get_search_form(); ?>
		
        <!-- Catalog -->
        <div class="catalog">
					
					<h1 class="catalog__title" style="margin: 15px 0 50px; text-align: center;">
						<?php echo the_title(  ); ?>
					</h1>
					
					
					
					<?php
						$company_price 					= get_field('company_price');
						$company_working_time 	= get_field('company_working_time');
						$all_company_addresses 	= get_field('all_company_addresses');
					?>
					
					
					<!-- Price -->
					<?php if( $company_price ) : ?>
						<div class="">
							<h3 class="">
								Прайс
							</h3>
							<p class="">
								<a href="<?php the_field('company_price'); ?>">
									Скачать прайс
								</a>
							</p>
						</div>
					<?php endif;?>
					
					<!-- Working time -->
					<?php if( $company_working_time ):  ?>
						<div class="">
							<h3 class="">
								Время работы
							</h3>
							<p class="">
								<?php echo (get_post_meta($post->ID, 'company_working_time', true)); ?>
							</p>
						</div>
					<?php endif;?>

					
					
					
					
					<div class="company-item company-office">
						<?php
							if( get_field('offices') ) {
								$counter = 0;
								
								
								// start while
								while( has_sub_field('offices') ) {
									$office_address = get_sub_field('office_address');
									$office_phone = get_sub_field('office_phone');
									$office_time = get_sub_field('office_time');
									++$counter;
									
									// If company has single office
									// console.log('single office');
									if ( $counter == 1 ) {
											$company_email 	= get_field('company_email'); 
											$website 				= get_field('company_website'); 
											$website_2 			= get_field('company_website_2'); 
											if( ($company_email) || ($website) || ($website_2) || ($office_address) || ($office_phone) || ($office_time) ):
										?>
											<div class="company-item company-contacts">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
														<h4 class="">Контакты</h4>
													</div>
												</div>
												
												<?php if( $office_address ):?>
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
															<p class="contacts-left">Адрес</p>
															<p class="contacts-right"><?php echo $office_address; ?></p>
														</div>
													</div>
												<?php endif;?>
												
												<?php if( $office_phone ):?>
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
															<p class="contacts-left">Телефон</p>
															<p class="contacts-right"><?php echo $office_phone; ?></p>
														</div>
													</div>
												<?php endif;?>
												
												<?php if( $office_time ):?>
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
															<p class="contacts-left">Время работы</p>
															<p class="contacts-right"><?php echo $office_time; ?></p>
														</div>
													</div>
												<?php endif;?>
													
												<?php 
													$company_email = get_field('company_email'); 
													if( $company_email ): 
												?>
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
															<p class="contacts-left">Email</p>
															<p class="contacts-right"><?php echo (get_post_meta($post->ID, 'company_email', true)); ?></p>
														</div>
													</div>
												<?php endif;?>
												
												<?php 
													$website = get_field('company_website'); 
													if( $website ): 
												?>
													<div class="website">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
																<p class="contacts-left">Сайт 1</p>
																<p class="contacts-right"><noindex><a rel="nofollow" target="_blank" class="" href="<?php echo (get_post_meta($post->ID, 'company_website', true)); ?>"><?php echo (get_post_meta($post->ID, 'company_website', true)); ?></a></noindex></p>
																<?php 
																	$website_2 = get_field('company_website_2'); 
																	if( $website_2 ): 
																?>
															</div>
														</div>
													</div>

													<div class="website">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">
																<p class="contacts-left">Сайт 2</p>
																<p class="contacts-right"><noindex><a rel="nofollow" target="_blank" class="" href="<?php echo (get_post_meta($post->ID, 'company_website_2', true)); ?>"><?php echo (get_post_meta($post->ID, 'company_website_2', true)); ?></a></noindex></p>
																<?php endif;?>
															</div>
														</div>
													</div>
												<?php endif;?>


												<?php 
													$vk = get_field('vk');
													$ok = get_field('ok');
													$fb = get_field('fb');
													$tw = get_field('tw');
													if( ($vk) || ($ok) || ($fb) || ($tw) ):
												?>
													<div class="social-networks">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12">

																<?php if( $vk ): ?>
																	<div class="social-networks-item">
																		<p class="contacts-left">Вконтакте</p>
																		<p class="contacts-right"><noindex><a rel="nofollow" target="_blank" class="" href="<?php echo (get_post_meta($post->ID, 'vk', true)); ?>"><?php echo (get_post_meta($post->ID, 'vk', true)); ?></a></noindex></p>
																	</div>
																<?php endif;?>

																<?php if( $ok ): ?>
																	<div class="social-networks-item">
																		<p class="contacts-left">Одноклассники</p>
																		<p class="contacts-right"><noindex><a rel="nofollow" target="_blank" class="" href="<?php echo (get_post_meta($post->ID, 'ok', true)); ?>"><?php echo (get_post_meta($post->ID, 'ok', true)); ?></a></noindex></p>
																	</div>
																<?php endif;?>

																<?php if( $fb ): ?>
																	<div class="social-networks-item">
																		<p class="contacts-left">Фэйсбук</p>
																		<p class="contacts-right"><noindex><a rel="nofollow" target="_blank" class="" href="<?php echo (get_post_meta($post->ID, 'fb', true)); ?>"><?php echo (get_post_meta($post->ID, 'fb', true)); ?></a></noindex></p>
																	</div>
																<?php endif;?>

																<?php if( $tw ): ?>
																	<div class="social-networks-item">
																		<p class="contacts-left">Твиттер</p>
																		<p class="contacts-right"><noindex><a rel="nofollow" target="_blank" class="" href="<?php echo (get_post_meta($post->ID, 'tw', true)); ?>"><?php echo (get_post_meta($post->ID, 'tw', true)); ?></a></noindex></p>
																	</div>
																<?php endif;?>
																
															</div>
														</div>
													</div>
												<?php endif;?>
												
												

											</div>
										<?php endif;
									}
									
									
									
									
									// If company has more than 1 office
									else {
								?>
										<div class='office-item'>
											<h4 class="">
												Офис №<?php echo ($counter - 1); ?>
											</h4>
										
											<?php if( $office_address ):?>
												<p class="contacts-left">
													Адрес
												</p>
												<p class="contacts-right">
													<?php echo $office_address; ?>
												</p>
											<?php endif;?>
										
											<?php if( $office_phone ):?>
												<p class="contacts-left">
													Телефон
												</p>
												<p class="contacts-right">
													<?php echo $office_phone; ?>
												</p>
											<?php endif;?>
													
											<?php if( $office_time ):?>
												<p class="contacts-left">
													Время работы
												</p>
												<p class="contacts-right">
													<?php echo $office_time; ?>
												</p>
											<?php endif;?>
										</div>
										
										
									<?php }
								}
								// end while
								
								
								
							}
						?>
					</div>

					
				
					<?php if( $all_company_addresses ) : ?>
						<div class="">
							<h3 class="">
								Все адреса и телефоны организации
							</h3>
							<p class="">
								<?php echo (get_post_meta($post->ID, 'all_company_addresses', true)); ?>
							</p>
						</div>
					<?php endif; ?>
						
						
						
						
						
						
						
						
					
					<div class="">
						<h2 class="clearfix">
							Адрес
						</h2>
						<p class="">
							г. Таганрог, <?php echo (get_post_meta($post->ID, 'company_address', true)); ?>
						</p>
						
						<div id="view1">
							<?php
							$location = get_field('company_map');
							if( ! empty($location) ):
							?>
							<div id="map" style="width: 100%; height: 300px;"></div>
								<script src='http://maps.googleapis.com/maps/api/js?sensor=false' type='text/javascript'></script>

								<script type="text/javascript">
									//<![CDATA[
									function load() {
										var lat = <?php echo $location['lat']; ?>;
										var lng = <?php echo $location['lng']; ?>;
										// coordinates to latLng
										var latlng = new google.maps.LatLng(lat, lng);
										// map Options
										var myOptions = {
											zoom: 14,
											center: latlng,
											scrollwheel: false,
											mapTypeId: google.maps.MapTypeId.ROADMAP
										};
										//draw a map
										var map = new google.maps.Map(document.getElementById("map"), myOptions);
										var marker = new google.maps.Marker({
										position: map.getCenter(),
										map: map
									});
									}
									// call the function
									load();
									//]]>
								</script>
							<?php endif; ?> 
						</div>
					</div>
					
					<br>
					
					<div class="descr">
						<h2 class="">
							Описание организации
						</h2>
						<div class="">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
		
      </div>
				
			<?php include 'news.php'; ?>
				
    </div>
<?php get_footer(); ?>
