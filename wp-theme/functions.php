<?php

/* Common styles */
if ( !is_admin() ) {
	wp_enqueue_style( 'dst-style-main',     get_template_directory_uri() . '/style.css');
	wp_enqueue_style( 'dst-style-plugins',  get_template_directory_uri() . '/css/plugins.css');
}

/* Favicon */
function website_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_template_directory_uri() .'/img/favicon/favicon.ico" />';
}
add_action('wp_head', 'website_favicon');

/* Delete jquery */
function wpdocs_dequeue_script() {
	wp_dequeue_script( 'jquery' ); 
} 
// add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

/* Reg scripts */
function js_custom_reg() {
	wp_register_script(
		'dst-plugins',
		get_template_directory_uri() . '/js/plugins.min.js'
	);
	wp_register_script(
		'dst-app',
		get_template_directory_uri() . '/js/app.js'
	);
}
add_action('init', 'js_custom_reg');

/* Run scripts */
function js_custom_run() {
	wp_enqueue_script( 'dst-plugins' );
	wp_enqueue_script( 'dst-app' );
}
add_action('wp_footer', 'js_custom_run');

/*
=========================================================================
*/

function ajax_posts() {
	add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
	function myajax_data(){

		wp_localize_script('dst-plugins', 'myajax',
			array(
				'url' => admin_url('admin-ajax.php')
			)
		);
	}

	add_action('wp_footer', 'my_action_javascript', 99);
	function my_action_javascript() {
?>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$clientsArr = [];
					$('#clients .list__item').each(function(){
						$thisId = $(this).attr('id');
						$clientsArr.push($thisId);
						$clientsString = $clientsArr.toString();
						return $clientsString;
					});
					var data = {
						action: 'my_action',
						result: $clientsString
					};
					

					jQuery.post( 
						myajax, 
						data, 
						function(response) {
							// console.log('Получено с сервера: ' + response);
							$clientSlickArgs = {
								infinite: 						true,
								init: 								true,
								autoplay: 						true,
								autoplaySpeed: 				2000,
								// RTL:									false,
								dots: 								false,
								arrows: 							false,
								unslick: 							false,
								responsive: [
									{
										breakpoint: 3000,
										settings: {
											vertical:				true,
											slidesToShow: 	5
										}
									},
									{
										breakpoint: 1024,
										settings: {
											vertical:				false,
											slidesToShow: 	3
										}
									},
									{
										breakpoint: 768,
										settings: {
											slidesToShow:		1
										}
									}
								]
							}
							$clients = $('#clients');
							// alert('Append?');
							$clients.append(response);
							// clients.append(response);
							// alert('Run slick?');
							$clients.slick($clientSlickArgs);
						}
					);
				});
			</script>
		<?php
	}

	add_action('wp_ajax_my_action', 'my_action_callback');
	add_action('wp_ajax_nopriv_my_action', 'my_action_callback');
	function my_action_callback() {
		$clientsExclude = $_POST['result'];
		
		$argsNewClients = array(
			'post_type'				=> 'company',
			'numberposts'			=> 45,
			'exclude'					=> $clientsExclude,
			'meta_key'				=> 'company_logotype',
			'orderby'					=> 'meta_value_num'
		);
		
		$newClients = get_posts( $argsNewClients );
		
		if( $newClients ) :
			foreach( $newClients as $pst ) : setup_postdata( $pst );
		
				$company_id		= $pst->ID;
				
				$meta_values 	= get_post_meta( $company_id, 'company_logotype' );
				$src 					= wp_get_attachment_image_src( $meta_values[0], 'thumbnail' );
				$imageSrc			= $src[0];
				
				$html .= 	
'<li class="list__item" id="'.$company_id.'">
<a class="widget_clients__link" href="'.get_permalink($company_id).'">
<img class="widget_clients__img" src="'.$imageSrc.'">
</a>
</li>';

			endforeach;
		endif;
		
		wp_reset_postdata();
		
		echo $html;
		wp_die();
	}
}
add_action('init', 'ajax_posts');


	
	
	
	