<?php
/*
Template Name: Home template
*/
get_header(); ?>

	<?php include 'svg/svg_catalog.php' ?>

	<?php include 'sidebar.php'; ?>
	
    <!-- Main content -->
    <div class="main-content">
		
			<?php include 'clients.php'; ?>
			
      <!-- Column -->
      <div class="column column_center">
			
        <!-- Search -->
				
				<!-- Widget center -->
				<div class="widget widget_center">
					<?php echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); ?>
				</div>
				
				<!-- Widget center -->
				<div class="widget widget_center">
				
					<h1 class="result-search" style="font-size: 22px; font-weight: 400;">
						<?php printf( __( 'Результаты поиска: %s' ), '' . get_search_query() . '' );?>
					</h1>
					
						<?php if ( have_posts() ) : ?>
							<ol>
								<?php while (have_posts()) : the_post(); ?>
									<?php 
										$curID = $post->ID;
										$postType = get_post_type( $curID );
									?>
									<?php if ($postType=='company' ) : ?>
										<li class="search-row" style="padding: 5px 0; font-size: 14px;">
											<a href="<?php the_permalink(); ?>">
												<?php the_title(); ?>
											</a>
										</li>
									<?php endif; ?>
								<?php endwhile; ?>
							</ol>
						<?php else: ?>
						<p>Извините, ничего не найдено...</p>
					<?php endif; ?>

					<?php
						global $wp_query;
						$big = 999999999;
						echo paginate_links( 
							array(
								'base' 			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format' 		=> '?paged=%#%',
								'current' 	=> max( 1, get_query_var('paged') ),
								'type' 			=> 'list',
								'prev_text' => __('« Назад'), 
								'next_text' => __('Вперёд »'),
								'total'			=> $wp_query->max_num_pages
							)
						);
					?>

				</div>
    </div>
					
			
			<?php include 'news.php'; ?>
			
    </div>
<?php get_footer(); ?>
