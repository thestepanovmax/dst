<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title></title>
    <?php wp_head();?>
	
</head>
<body>
<!-- Container -->
<div class="container">
	<?php include 'header_top.php' ?>
	<?php include 'header_bottom.php' ?>
	<?php include 'svg/svg_menu.php' ?>
	<?php include 'svg/svg_search.php' ?>