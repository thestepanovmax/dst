
        <div class="info">
          <h3 class="info__title">
            Рекламно-информационный комплекс «Деловая справка» — это интерактивное обеспечение информацией потенциальных потребителей товаров и услуг номер телефона 31-000-9.
          </h3>
          <p>
            В современном мире Информация часто является одной из самых важных составляющих бизнеса и играет важную роль в жизни любого человека. А такая информация, как деловые контакты, адреса и телефоны необходимых людей, компаний, государственных организаций, часто необходима здесь и сейчас. РИК «Деловая справка» (справочная по товарам и услугам г. Таганрога) поможет Вам быстро и удобно найти необходимую информацию.
          </p>
          <p>
            В сети по адресу http://www.dstaganrog.ru   размещена  удобная поисковая  система, предназначенная для самостоятельного круглосуточного поиска товаров и услуг. Становясь активным пользователем сайта, размещая информацию о своей компании, предприятии или фирме на его страницах, вы не только существенно облегчите себе ведение бизнеса, но и получите массу информации, актуальной каждый день для любого активного гражданина страны.
          </p>
          <p>
            Информационно-рекламный Комплекс «Деловая справка» (справочная по товарам и услугам)  — уникальный для города ресурс, позволяющий предприятиям различных форм собственности при небольших вложениях решить задачи постоянного и эффективного продвижения своих товаров и услуг.
          </p>
          <p>
            Благодаря своей доступности для всех слоев населения, Комплекс пользуется большой популярностью. Мы содействуем повышению интереса к обслуживаемым предприятиям, а значит и росту их выручки.
          </p>
          <p>
            Приглашаем все заинтересованные в своем развитии предприятия к взаимовыгодному сотрудничеству!
          </p>
        </div>