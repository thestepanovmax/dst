<?php include 'menu-mobile.php'; ?>

<!-- Sidebar -->
<aside class="sidebar" id="sidebar">
<!-- Call-us -->
<div class="call-us">
  <h3 class="call-us__title">
    Узнайте информацию первым!
  </h3>
  <a class="call-us__link" href="tel:88634310009" title="Звоните нам прямо сейчас!">
    <svg class="call-us__icon">
      <use xlink:href="#icon-phone"></use>
    </svg>
    <span class="call-us__number">
      8 (8634) 31-000-9
    </span>
  </a>
</div>
<?php include 'menu-desktop.php'; ?>
<!-- Developer -->
<div class="developer">
  <div class="developer__info"></div>
  <a href="http://stepanovmax.ru" title="" class="developer__link">
    <svg class="developer__icon">
      <use xlink:href="#icon-code"></use>
    </svg>
    <span class="developer__text">Разработчик сайта</span>
  </a>
</div>
</aside>