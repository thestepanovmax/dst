<form role="search" method="get" class="search searchform group" action="<?php echo home_url( '/' ); ?>">
	<div class="form__row">
		<svg class="search__icon">
			<use xlink:href="#icon-search-file"></use>
		</svg>
		<input id="search"
			class="search__field"
			type="search" 
			placeholder="<?php echo esc_attr_x( 'Поиск', 'placeholder' ) ?>"
			value="<?php echo get_search_query() ?>" name="s"
			title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<button class="btn btn_search search__btn">
			<svg class="btn__icon icon-search">
				<use xlink:href="#icon-search"></use>
			</svg>
		</button>
	</div>
</form>