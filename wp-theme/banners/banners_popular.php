<!-- Widget popular -->
<div class="widget widget_popular">
	<h3 class="widget__title">Наиболее популярные предложения:</h3>
 
	<?php

		$topPosts = get_posts(
			array(
				'post_type'			=> array( 'company', 'only_banner'),
				'posts_per_page'	=> -1,
				'meta_key'			=> 'banner_top',
				'order'				=> 'ASC',
				'meta_key'			=> 'priority',
				'orderby'			=> 'meta_value_num'
      )
		);

		if( $posts ): ?>
			<ul class="list list_horizontal slider" id="clients_popular">		
				<?php
					foreach( $topPosts as $post ): 		
						setup_postdata( $post )
				?>
					<?php 
						$priority 			= get_field( "priority" );
						$bannerTop 			= get_field( "banner_top" );
						$banner_top_image	= get_field( "company_logotype" );

						if( $bannerTop ): 
					?>
						<li class="list__item">
								<a  href="<?php the_permalink(); ?>" 
										title="<?php echo $banner_top_image['title']; ?>" 
										class="widget__link">
									<img class="widget__img <?php echo $priority; ?>"
										 src="<?php echo $banner_top_image['url']; ?>"
										 alt="<?php echo $banner_top_image['alt']; ?>"
										 title="<?php echo $banner_top_image['title']; ?>"
										 width="<?php echo $banner_top_image['width']; ?>"
										 height="<?php echo $banner_top_image['height']; ?>">
							 </a>
							
						</li>
					<?php endif; ?>
				<?php endforeach; ?>	
			</ul>
		<?php endif; ?>
		
	<?php wp_reset_postdata(); ?>

</div>