<?php 
	$topBanners = get_posts(array(
		'post_type'				=> 'banners',
		'posts_per_page'	=> -1,
		'meta_key'				=> 'banner_options',
		'order'						=> 'DESC'
	)); 
?>

<?php if( $topBanners ): ?>
	<?php
		foreach( $topBanners as $post ):
			setup_postdata( $post )
	?>
		<?php 
			$banner_options = get_field('banner_options'); 
			if( $banner_options ): 
			$numBanners = count(get_field('banner_options')); 
		?>
		
			<?php if ( $numBanners == 1 ) { ?>
				<?php while( has_sub_field('banner_options') ) { ?>
					<?php 
						$banner_image 	= get_sub_field('banner_image');
						$link_on_banner = get_sub_field('link_on_banner');
					?>
					<a class="header-top__link" href='<?php echo $link_on_banner; ?>'>
						<img class="header-top__img" src="<?php echo $banner_image; ?>">
					</a>
				<?php } ?>
			<?php } ?>
		
			<?php if ( $numBanners == 2 ) { ?>
				<?php 
					while( has_sub_field('banner_options') ) { ?>
					<?php 
						$banner_image 	= get_sub_field('banner_image');
						$link_on_banner = get_sub_field('link_on_banner');
						$size 				= 'full';
						$imageUrl 			= $banner_image['url'];
						$imageAlt 			= $banner_image['alt'];
						$imageTitle 		= $banner_image['title'];
						$imageWidth 		= $banner_image['width'];
						$imageHeight 		= $banner_image['height'];
					?>
					<div class='column column_half'>
						<a class="header-top__link" href='<?php echo $link_on_banner ?>'>
							<img 
							    class="header-top__img"
								src="<?php echo $imageUrl ?>" 
								title="<?php echo $imageTitle; ?>" 
								alt="<?php echo $imageAlt; ?>" 
								width="<?php echo $imageWidth; ?>" 
								height="<?php echo $imageHeight; ?>">
						</a>
					</div>
					
				<?php } ?>
			<?php } ?>
			
			<?php if ( $numBanners == 3 ) { ?>
				<div class='row'>
					<?php while( has_sub_field('banner_options') ) { ?>
						<?php 
							$banner_image 	= get_sub_field('banner_image');
							$link_on_banner = get_sub_field('link_on_banner');
						?>
						<div class='column column_half'>
							<a class="header-top__link" href='<?php echo $link_on_banner ?>'>
								<img class="header-top__img" src='<?php echo $banner_image ?>'>
							</a>
						</div>
					
					<?php } ?>
				</div>
			<?php } ?>
			
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>

<?php wp_reset_postdata(); ?>
