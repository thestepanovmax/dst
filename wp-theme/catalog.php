
          <h2 class="catalog__title">
            Каталог организаций и предприятий
          </h2>
          <ul class="list list_horizontal">
            <li class="list__item">
              <a href="#" class="catalog__link" title="Аварийные, экстренные службы">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-avaryinye-sluzhby"></use>
                </svg>
                <span class="catalog__text">
                  Аварийные, экстренные службы
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Аварийные, экстренные службы">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-avtotransportnye-tovary-i-uslugi"></use>
                </svg>
                <span class="catalog__text">
                  Автотранспортные товары и услуги
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Безопасность и защита">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-bezopasnost-zaschita"></use>
                </svg>
                <span class="catalog__text">
                  Безопасность и защита
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Бизнес, финансы и юриспруденция">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-biznes-finansy-urisprudencia"></use>
                </svg>
                <span class="catalog__text">
                  Бизнес, финансы и юриспруденция
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Бытовые услуги">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-bytovye-uslugi"></use>
                </svg>
                <span class="catalog__text">
                  Бытовые услуги
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Все для строительства и ремонта">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-vse-dly-stroitelstva-remonta"></use>
                </svg>
                <span class="catalog__text">
                  Все для строительства и ремонта
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Государственные медицинские учреждения">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-gos-med-uchrezhdeniya"></use>
                </svg>
                <span class="catalog__text">
                  Государственные медицинские учреждения
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Госучереждения и общественные организации">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-gos-organizacii"></use>
                </svg>
                <span class="catalog__text">
                  Госучереждения и общественные организации
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Дети">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-children"></use>
                </svg>
                <span class="catalog__text">
                  Дети
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Животный мир и флористика">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-zhivotnye-mir-i-floristika"></use>
                </svg>
                <span class="catalog__text">
                  Животный мир и флористика
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Красота и здоровье">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-krasota-i-zdorove"></use>
                </svg>
                <span class="catalog__text">
                  Красота и здоровье
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Медицина и фармацевтика">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-medicina-i-farmacevtika"></use>
                </svg>
                <span class="catalog__text">
                  Медицина и фармацевтика
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Недвижимость">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-nedvizhimost"></use>
                </svg>
                <span class="catalog__text">
                  Недвижимость
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Образование и наука">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-gos-obrazovat-uchrezhdeniya"></use>
                </svg>
                <span class="catalog__text">
                  Образование и наука
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Отдых, досуг, развлечения">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-otdyh-dosug-razvlechenie"></use>
                </svg>
                <span class="catalog__text">
                  Отдых, досуг, развлечения
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Промышленность и производство">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-promyshlennost-proizvodstvo"></use>
                </svg>
                <span class="catalog__text">
                  Промышленность и производство
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Реклама и полиграфия, СМИ">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-reklama-i-poligrafiya-smi"></use>
                </svg>
                <span class="catalog__text">
                  Реклама и полиграфия, СМИ
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Спорт и фитнес">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-sport-i-fitnes"></use>
                </svg>
                <span class="catalog__text">
                  Спорт и фитнес
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Строительные и ремонтные работы">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-stroitelnye-i-remontnye-raboty"></use>
                </svg>
                <span class="catalog__text">
                  Строительные и ремонтные работы
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Телекоммуникации и связь">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-telekommunikacii-i-svyaz"></use>
                </svg>
                <span class="catalog__text">
                  Телекоммуникации и связь
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Товары">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-tovary"></use>
                </svg>
                <span class="catalog__text">
                  Товары
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Транспортные услуги и грузоперевозки">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-transportnye-uslugi-i-perevozki"></use>
                </svg>
                <span class="catalog__text">
                  Транспортные услуги и грузоперевозки
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Туризм">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-turism"></use>
                </svg>
                <span class="catalog__text">
                  Туризм
                </span>
              </a>
            </li>
            <li class="list__item">
              <a href="#" class="catalog__link" title="Экология">
                <svg class="catalog__icon">
                  <use xlink:href="#icon-akologiy"></use>
                </svg>
                <span class="catalog__text">
                  Экология
                </span>
              </a>
            </li>
          </ul>