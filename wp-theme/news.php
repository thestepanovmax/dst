
<!-- Widget news -->
<div class="widget widget_news">
	<h3 class="widget__title">
		Наши новости
	</h3>
	<!-- List -->
	<ul class="list list_vertical slider">
		<li class="list__item">
			<h4 class="widget_news__title">
				<a class="widget_news__link" href="#" title="«Добродея» Бюро услуг">
					«МиЛеди» магазин женской одежды
				</a>
			</h4>
			<p class="widget_news__descr">
				«Добродея» Предлагает широкий спектр услуг. Няни, гувернантки, репетиторы. Домработницы (генеральная
			</p>
			<p class="widget_news__date">
				21.07.2016
			</p>
		</li>
		<li class="list__item">
			<h4 class="widget_news__title">
				<a class="widget_news__link" href="#" title="«Добродея» Бюро услуг">
					«МиЛеди» магазин женской одежды
				</a>
			</h4>
			<p class="widget_news__descr">
				«Добродея» Предлагает широкий спектр услуг. Няни, гувернантки, репетиторы. Домработницы (генеральная
			</p>
			<p class="widget_news__date">
				21.07.2016
			</p>
		</li>
	</ul>
</div>