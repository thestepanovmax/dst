<?php
	$posts = get_posts(
		array(
			'post_type'			=> 'company',
			'posts_per_page'	=> -1,
			'meta_key'			=> 'company_logotype',
			'orderby'			=> 'meta_value_num',
			'order'				=> 'DESC'
		)
	);
	if( $posts ): ?>
	
	<?php
		foreach( $posts as $post ): 		
			setup_postdata( $post )		
	?>
		<?php 
			$logotype 				= 	get_field('company_logotype');
			$banner_top_image		= 	get_field('company_logotype');
			$size 					= 	'thumbnail';
			$image 					= 	wp_get_attachment_image_src( $banner_top_image, $size );
			$logotype_url 			=	$logotype['url'];
			$banner_top_url_thumb 	=	$logotype['sizes']['thumbnail'];
			$logotype_alt 			=	$logotype['alt'];
			$logotype_title 		=	$logotype['title'];
			$logotype_width 		=	$logotype['width'];
			$logotype_height 		=	$logotype['height'];
			if( $logotype ): 
		?>
			<li class="list__item">
				<a class="widget_clients__link" 
					href="<?php the_permalink(); ?>" 
					title="<?php echo $logotype_alt; ?>">
						<img class="widget_clients__img"
							src="<?php echo $logotype_url; ?>" 
							alt="<?php echo $logotype_alt; ?>" 
							title="<?php echo $logotype_title; ?>" 
							width="<?php echo $logotype_width; ?>" 
							height="<?php echo $logotype_height; ?>">
				</a>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
	
<?php endif; ?>
<?php wp_reset_postdata(); ?>