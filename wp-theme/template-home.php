<?php
/*
Template Name: Home template
*/
get_header(); ?>

	<?php include 'svg/svg_catalog.php' ?>

	<?php include 'sidebar.php'; ?>
	
    <!-- Main content -->
    <div class="main-content">
		
			<?php include 'clients.php'; ?>
			
      <!-- Column -->
      <div class="column column_center">
			
				<!-- Widget center -->
				<div class="widget widget_center">
					<!-- Search -->
					<?php echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); ?>
				</div>
				
        <!-- Catalog -->
        <div class="catalog">
					<?php include 'catalog.php'; ?>
        </div>

      	<!-- Info -->
				<?php include 'info.php'; ?>
				
      </div>
			
			<?php include 'news.php'; ?>
			
    </div>
<?php get_footer(); ?>
