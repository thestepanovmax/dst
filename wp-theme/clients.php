
<!-- Widget popular -->
<div class="widget widget_clients">
	<h3 class="widget__title">
		Наши клиенты
	</h3>
	<?php
	
		$argsClients = array(
				'post_type'				=> 'company',
				'numberposts'			=> 5,
				'meta_key'				=> 'company_logotype',
				'orderby'					=> 'meta_value_num'
		);
		$clients = get_posts( $argsClients );
		if( $clients ): ?>
			
			<ul class="list list_vertical slider" id="clients">	
				<?php
					foreach( $clients as $post ): 		
						setup_postdata( $post )
				?>
					<?php 
						$company_id 						= $post->ID;
						$logotype 							= get_field('company_logotype');
						$logotype_url 					= $logotype['url'];
						$logotype_src 					= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' );
						
						$banner_top_url_thumb 	= $logotype['sizes']['thumbnail'];
						$logotype_alt 					= $logotype['alt'];
						$logotype_title 				= $logotype['title'];
						$logotype_width 				= $logotype['width'];
						$logotype_height 				= $logotype['height'];
						
						if( $logotype ):
						
						
						// $meta_values = get_post_meta( $company_id, 'company_logotype' );
						// $src = wp_get_attachment_image_src( $meta_values[0], 'thumbnail' );
						
					?>
						<li class="list__item" id="<?php echo $company_id; ?>">
							<span><?php // print_r( $src[0] ); ?></span>
							<a 	class="widget_clients__link" 
									href="<?php the_permalink(); ?>"
									title="<?php echo $logotype_alt; ?>">
										<img 	class="widget_clients__img"
													src="<?php echo $logotype['sizes']['medium'] ?>" 
													alt="<?php echo $logotype_alt; ?>" 
													title="<?php echo $logotype_title; ?>" 
													width="<?php echo $logotype_width; ?>" 
													height="<?php echo $logotype_height; ?>">
							</a>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		
	<?php wp_reset_postdata(); ?>
			
</div>